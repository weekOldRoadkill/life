// Modules
mod creature;
mod creatures;
mod state;
mod util;

// Namespaces
use crate::util::*;

// Main function
fn main() -> Result {
    let (context, event_loop) = ContextBuilder::new("Life", "weekOldRoadkill")
        .default_conf(conf::Conf {
            window_mode: conf::WindowMode {
                maximized: true,
                resizable: true,
                ..Default::default()
            },
            window_setup: conf::WindowSetup {
                title: "Life".into(),
                samples: conf::NumSamples::Four,
                ..Default::default()
            },
            ..Default::default()
        })
        .build()?;
    let state = state::State::new(&context)?;
    event::run(context, event_loop, state);
}
