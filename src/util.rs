// Namespaces
pub(crate) use crate::*;
pub use ggez::*;
pub use glam::f32::*;
pub use rand::prelude::*;
pub use std::*;

// Error enum
#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("GGez error: {0}")]
    GGezError(#[from] GameError),
    #[error("{0:?} is not bound")]
    KeyNotBound(input::keyboard::KeyInput),
    #[error("Empty range")]
    EmptyRange,
}

// Result type
pub type Result<T = ()> = result::Result<T, Error>;

// Generate a new unique ID
pub fn new_id() -> usize {
    static ID: sync::atomic::AtomicUsize = sync::atomic::AtomicUsize::new(0);
    ID.fetch_add(1, sync::atomic::Ordering::Relaxed)
}
