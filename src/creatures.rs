// Namespaces
use crate::util::*;

// Creatures struct
pub struct Creatures {
    creatures: Vec<creature::Creature>,
    mesh: graphics::Mesh,
    instances: graphics::InstanceArray,
}

// Creatures implementation
impl Creatures {
    // Create a new set of creatures
    pub fn new_random(
        context: &Context,
        mass_range: ops::Range<f32>,
        pos_ranges: (ops::Range<f32>, ops::Range<f32>),
        count: usize,
    ) -> Result<Self> {
        Ok(Self {
            creatures: (0..count)
                .map(|_| creature::Creature::new_random(mass_range.clone(), pos_ranges.clone()))
                .collect::<Result<_>>()?,
            mesh: graphics::Mesh::from_data(
                context,
                graphics::MeshBuilder::new()
                    .circle(
                        graphics::DrawMode::fill(),
                        Vec2::ZERO,
                        1.0,
                        1.0 / 32.0,
                        graphics::Color::WHITE,
                    )?
                    .build(),
            ),
            instances: graphics::InstanceArray::new(context, None),
        })
    }

    // Update the creatures
    pub fn update(self: &mut Self, delta: f32) -> () {
        self.creatures = self
            .creatures
            .iter()
            .cloned()
            .map(|mut creature| {
                creature.run_input(delta, creature::Input::Eat);
                creature.apply_drag(delta);
                creature.apply_vel(delta);
                creature.metabolize(delta);
                creature
            })
            .filter(|creature| !creature.should_die())
            .collect();
        self.reproduce();
    }

    // Draw the creatures
    pub fn draw(
        self: &mut Self,
        canvas: &mut graphics::Canvas,
        screen_coords: &graphics::Rect,
    ) -> () {
        self.instances.clear();
        self.creatures
            .iter()
            .filter(|creature| screen_coords.overlaps_circle(creature.pos(), creature.radius()))
            .map(|creature| {
                let color = match creature.feeding_strat() {
                    creature::FeedingStrat::Autotrophy => graphics::Color::new(
                        0xB8 as f32 / 255.0,
                        0xBB as f32 / 255.0,
                        0x26 as f32 / 255.0,
                        1.0,
                    ),
                    creature::FeedingStrat::Heterotrophy => graphics::Color::new(
                        0xFB as f32 / 255.0,
                        0x49 as f32 / 255.0,
                        0x34 as f32 / 255.0,
                        1.0,
                    ),
                };
                graphics::DrawParam::new()
                    .dest(creature.pos())
                    .color(graphics::Color {
                        a: (2.0 * creature.energy() / creature.mass()).clamp(0.1, 1.0),
                        ..color
                    })
                    .scale(Vec2::splat(creature.radius()))
            })
            .for_each(|draw_param| self.instances.push(draw_param));
        canvas.draw_instanced_mesh(
            self.mesh.clone(),
            &self.instances,
            graphics::DrawParam::new(),
        );
    }

    // Reproduce the creatures
    fn reproduce(self: &mut Self) -> () {
        for i in 0..self.creatures.len() {
            if self.creatures[i].should_reproduce() && self.creatures.len() < 25000 {
                self.creatures[i].reset_energy();
                let mut creature = self.creatures[i].clone();
                creature.mutate();
                self.creatures.push(creature);
            }
        }
    }

    // Get the size of the creatures
    pub fn size(self: &Self) -> usize {
        self.creatures.len()
    }
}
