// Namespaces
use crate::util::*;

// State struct
pub struct State {
    scale: f32,
    pos: Vec2,
    screen_coords: graphics::Rect,
    creatures: creatures::Creatures,
}

// State implementation
impl State {
    // Create a new state
    pub fn new(context: &Context) -> Result<Self> {
        let mut state = Self {
            scale: Default::default(),
            pos: Default::default(),
            screen_coords: Default::default(),
            creatures: creatures::Creatures::new_random(
                context,
                0.00008..0.0003,
                (-1.0..1.0, -1.0..1.0),
                1000,
            )?,
        };
        state.set_scale_pos(context, 1.0, Vec2::ZERO);
        Ok(state)
    }

    // Set the scale and position
    fn set_scale_pos(self: &mut Self, context: &Context, scale: f32, pos: Vec2) -> () {
        let dims: Vec2 = context.gfx.drawable_size().into();
        let half_scale = scale * dims / dims.min_element();
        self.scale = scale;
        self.pos = pos;
        self.screen_coords = graphics::Rect::new(
            -half_scale.x + pos.x,
            -half_scale.y + pos.y,
            2.0 * half_scale.x,
            2.0 * half_scale.y,
        );
    }
}

// Event handler implementation
impl event::EventHandler<Error> for State {
    // Update the state
    fn update(self: &mut Self, context: &mut Context) -> Result {
        self.creatures.update(context.time.delta().as_secs_f32());
        Ok(())
    }

    // Draw the state
    fn draw(self: &mut Self, context: &mut Context) -> Result {
        let mut canvas = graphics::Canvas::from_frame(
            context,
            graphics::Color::new(
                0x1D as f32 / 255.0,
                0x20 as f32 / 255.0,
                0x21 as f32 / 255.0,
                1.0,
            ),
        );
        canvas.set_screen_coordinates(self.screen_coords);
        self.creatures.draw(&mut canvas, &self.screen_coords);
        canvas.draw(
            graphics::Text::new(format!("Creatures: {}", self.creatures.size())).set_scale(32.0),
            graphics::DrawParam::new()
                .dest(Vec2::from(self.screen_coords.point()) + self.scale * Vec2::new(0.02, 0.01))
                .scale(Vec2::splat(0.0015 * self.scale)),
        );
        canvas.finish(context)?;
        Ok(())
    }

    // Handle mouse motion events
    fn mouse_motion_event(
        self: &mut Self,
        context: &mut Context,
        _x: f32,
        _y: f32,
        dx: f32,
        dy: f32,
    ) -> Result {
        if context
            .mouse
            .button_pressed(input::mouse::MouseButton::Left)
        {
            let dpos = Vec2::from(self.screen_coords.size()) * Vec2::new(dx, dy)
                / Vec2::from(context.gfx.drawable_size());
            self.set_scale_pos(context, self.scale, self.pos - dpos);
        }
        Ok(())
    }

    // Handle mouse wheel events
    fn mouse_wheel_event(self: &mut Self, context: &mut Context, _x: f32, y: f32) -> Result {
        self.set_scale_pos(context, self.scale * (0.1 * y + 1.0), self.pos);
        Ok(())
    }

    // Handle key down events
    fn key_down_event(
        self: &mut Self,
        context: &mut Context,
        input: input::keyboard::KeyInput,
        repeated: bool,
    ) -> Result {
        match input.keycode {
            Some(input::keyboard::KeyCode::Escape) if !repeated => {
                context.request_quit();
                Ok(())
            }
            _ => Err(Error::KeyNotBound(input)),
        }
    }

    // Handle resize events
    fn resize_event(self: &mut Self, context: &mut Context, _width: f32, _height: f32) -> Result {
        self.set_scale_pos(context, self.scale, self.pos);
        Ok(())
    }

    // Handle errors
    fn on_error(
        self: &mut Self,
        _context: &mut Context,
        _origin: event::ErrorOrigin,
        error: Error,
    ) -> bool {
        match error {
            Error::KeyNotBound(_) => {
                println!("Nonfatal: {error}\n");
                false
            }
            _ => {
                println!("Fatal: {error}\n");
                true
            }
        }
    }
}
