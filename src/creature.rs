// Namespaces
use crate::util::*;

// Feeding strategy enum
#[derive(Clone)]
pub enum FeedingStrat {
    Autotrophy,
    Heterotrophy,
}

// Distribution implementation for FeedingStrat
impl Distribution<FeedingStrat> for rand::distributions::Standard {
    fn sample<R: rand::Rng + ?Sized>(&self, rng: &mut R) -> FeedingStrat {
        match rng.gen_range(0..2) {
            0 => FeedingStrat::Autotrophy,
            _ => FeedingStrat::Heterotrophy,
        }
    }
}

// Input enum
pub enum Input {
    None,
    Move(Vec2),
    Eat,
}

// Creature struct
#[derive(Clone)]
pub struct Creature {
    id: usize,
    mass: f32,
    radius: f32,
    feeding_strat: FeedingStrat,
    pos: Vec2,
    vel: Vec2,
    energy: f32,
}

// Creature implementation
impl Creature {
    // Create a new random creature
    pub fn new_random(
        mass_range: ops::Range<f32>,
        pos_ranges: (ops::Range<f32>, ops::Range<f32>),
    ) -> Result<Self> {
        if mass_range.is_empty() || pos_ranges.0.is_empty() || pos_ranges.1.is_empty() {
            Err(Error::EmptyRange)
        } else {
            let mut rng = thread_rng();
            let mut creature = Self {
                id: new_id(),
                mass: Default::default(),
                radius: Default::default(),
                feeding_strat: rng.gen(),
                pos: Vec2::new(rng.gen_range(pos_ranges.0), rng.gen_range(pos_ranges.1)),
                vel: Vec2::ZERO,
                energy: Default::default(),
            };
            creature.set_mass(rng.gen_range(mass_range));
            Ok(creature)
        }
    }

    // Getters
    pub fn id(self: &Self) -> usize {
        self.id
    }
    pub fn mass(self: &Self) -> f32 {
        self.mass
    }
    pub fn radius(self: &Self) -> f32 {
        self.radius
    }
    pub fn feeding_strat(self: &Self) -> FeedingStrat {
        self.feeding_strat.clone()
    }
    pub fn pos(self: &Self) -> Vec2 {
        self.pos
    }
    pub fn vel(self: &Self) -> Vec2 {
        self.vel
    }
    pub fn energy(self: &Self) -> f32 {
        self.energy
    }

    // Setters
    fn set_mass(self: &mut Self, mass: f32) -> () {
        self.mass = mass.max(0.00002);
        self.radius = (self.mass / f32::consts::PI).sqrt();
        self.energy = 0.5 * self.mass;
    }

    // Apply drag to the creature
    pub fn apply_drag(self: &mut Self, delta: f32) -> () {
        const DENSITY: f32 = 0.5;
        const DRAG_COEFF: f32 = 0.5;
        let mag_squared = self.vel.length_squared();
        let reference_area = 2.0 * self.radius;
        let dir = -self.vel.normalize_or_zero();
        self.apply_force(
            delta,
            0.5 * DENSITY * mag_squared * DRAG_COEFF * reference_area * dir,
        );
    }

    // Apply a force to the creature
    fn apply_force(self: &mut Self, delta: f32, force: Vec2) -> () {
        self.apply_impulse(delta * force);
    }

    // Apply an impulse to the creature
    fn apply_impulse(self: &mut Self, impulse: Vec2) -> () {
        self.vel += impulse / self.mass;
    }

    // Apply the velocity to the creature
    pub fn apply_vel(self: &mut Self, delta: f32) -> () {
        self.pos += delta * self.vel;
    }

    // Consume energy from the creature
    fn consume_energy(self: &mut Self, energy: f32) -> () {
        self.energy -= energy;
    }

    // Consume energy from the creature over time
    fn consume_energy_delta(self: &mut Self, delta: f32, rate: f32) -> () {
        self.consume_energy(delta * rate);
    }

    // Check if the creature should die
    pub fn should_die(self: &Self) -> bool {
        self.energy <= 0.0
    }

    // Check if the creature should reproduce
    pub fn should_reproduce(self: &Self) -> bool {
        self.energy >= self.mass
    }

    // Metabolize the creature
    pub fn metabolize(self: &mut Self, delta: f32) -> () {
        self.consume_energy_delta(delta, 0.05 * self.mass);
    }

    // Mutate the creature
    pub fn mutate(self: &mut Self) -> () {
        let mut rng = thread_rng();
        self.id = new_id();
        self.pos += Vec2::new(rng.gen_range(-0.01..0.01), rng.gen_range(-0.01..0.01));
        self.set_mass(self.mass + rng.gen_range(-0.00002..0.00002));
    }

    // Reset the energy of the creature
    pub fn reset_energy(self: &mut Self) -> () {
        self.set_mass(self.mass);
    }

    // Run input on the creature
    pub fn run_input(self: &mut Self, delta: f32, input: Input) -> () {
        match input {
            Input::None => (),
            Input::Move(mut force) => {
                force = force.clamp_length_max(0.000005);
                self.apply_force(delta, force);
                self.consume_energy_delta(delta, 0.5 * force.length());
            }
            Input::Eat => match self.feeding_strat {
                FeedingStrat::Autotrophy => self.consume_energy_delta(delta, -0.075 * self.mass),
                FeedingStrat::Heterotrophy => (),
            },
        }
    }
}
